import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  StyleSheet,
} from 'react-native';

const Button = ({title, buttonStyle, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.buttonContainer, buttonStyle]}>
        <Text style={styles.text}>{title}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    backgroundColor: '#6638f0',
    padding: 10,
    borderRadius: 10,
  },
  text: {
    fontSize: 15,
    color: '#ffffe6',
    textAlign: 'center',
  },
});

export default Button;
