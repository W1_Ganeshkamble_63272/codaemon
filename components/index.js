// import all the components
import Button from './button';
import AppInputText from './appInputText';

// export all the components
export {Button, AppInputText};
