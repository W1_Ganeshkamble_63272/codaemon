import React from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';

const AppInputText = ({title, placeholder, onChange, secureText = false}) => {
  return (
    <View>
      <Text style={styles.title}>{title}</Text>
      <TextInput
        secureTextEntry={secureText}
        onChangeText={onChange}
        placeholder={placeholder}
        style={styles.input}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
  },
  input: {
    borderStyle: 'solid',
    borderWidth: 2,
    borderColor: '#000000',
    height: 45,
    marginVertical: 10,
    borderRadius: 10,
    paddingHorizontal: 20,
    backgroundColor: '#e6ffff',
  },
});

export default AppInputText;
